<link rel="stylesheet" href="css/style.css">
<?php include "lib/layouts/header.php";?>
<?php include "lib/layouts/nav.php";?>


<div class="home-img"></div>
<div class="home-title">
    Quizzes are in following Topics
</div>
<div class="home-grid">
    <div class="grid-item1">In Environment</div>
    <div class="grid-item2">In Automobile </div>
    <div class="grid-item3">In Maths</div>
    <div class="grid-item4">In Robotics</div>
    <div class="grid-item5">In Sports</div>
    <div class="grid-item6">In Pollution </div>
    <div class="grid-item7">In IQ</div>
    <div class="grid-item8">In IT</div>
</div>
<div class="comment">
    <div class="title">
        Comments from users
    </div>
    <div class="body">
        <div class="comment-grid">
            <div class="comment1">
                <div class="usern">JehanKandy</div>
                <div class="user-roll">Admin</div>
                <div class="comment-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi blanditiis ut cumque quasi dolorum magnam dolores, assumenda vero repellat cupiditate dolore ratione, illum distinctio pariatur. Dignissimos quia pariatur soluta molestias?
                </div>
            </div>
            <div class="comment2">
                <div class="usern">JehanKandy</div>
                <div class="user-roll">Admin</div>
                <div class="comment-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi blanditiis ut cumque quasi dolorum magnam dolores, assumenda vero repellat cupiditate dolore ratione, illum distinctio pariatur. Dignissimos quia pariatur soluta molestias?
                </div>
            </div>
            <div class="comment3">
                <div class="usern">JehanKandy</div>
                <div class="user-roll">Admin</div>
                <div class="comment-body">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi blanditiis ut cumque quasi dolorum magnam dolores, assumenda vero repellat cupiditate dolore ratione, illum distinctio pariatur. Dignissimos quia pariatur soluta molestias?
                </div>
            </div>
        </div>
    </div>
</div>
<div class="partner">
    <div class="title">PARTNERSHIP</div>
    <div class="partner-grid">
        <div class="p-item1"></div>
        <div class="p-item2"></div>
        <div class="p-item3"></div>
        <div class="p-item4"></div>
        <div class="p-item5"></div>
        <div class="p-item6"></div>
    </div>
</div>

<script src="js/script.js"></script>
<?php include "lib/layouts/home_footer.php";?>
